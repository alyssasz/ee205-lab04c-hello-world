///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04c - Hello World 
///
/// @file hello1.cpp
/// @version 1.0
///
/// Program 1 of Hello World
///
/// @author Alyssa Zhang <alyssasz@hawaii.edu>
/// @brief  Lab 04c - Hello World - EE 205 - Spr 2021
/// @date   Feb 11 2021
///////////////////////////////////////////////////////////////////////////////
#include <iostream>

using namespace std;

int main() {

    cout << "Hello World!" << endl; 

}

