///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04c - Hello World 
///
/// @file hello2.cpp
/// @version 1.0
///
/// Program 2 of Hello World
///
/// @author Alyssa Zhang <alyssasz@hawaii.edu>
/// @brief  Lab 04c - Hello World - EE 205 - Spr 2021
/// @date   Feb 11 2021
///////////////////////////////////////////////////////////////////////////////
#include <iostream>

int main() {
    
    std:: cout << "Hello World!" << std:: endl;

}




